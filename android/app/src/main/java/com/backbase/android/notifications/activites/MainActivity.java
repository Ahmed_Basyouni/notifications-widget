package com.backbase.android.PACKAGE_NAME.activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backbase.android.PACKAGE_NAME.R;
import com.backbase.android.PACKAGE_NAME.CustomRenderable;
import com.backbase.android.widget.PACKAGE_NAME.core.COMPONENT_NAMEWidget;

import android.view.ViewGroup;

/**
 * Created by Backbase R&D B.V on DATE_HOLDER.
 */
public class MainActivity extends AppCompatActivity {

    private COMPONENT_NAMEWidget renderer;
    private CustomRenderable renderableItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        renderableItem = new CustomRenderable("DefaultCOMPONENT_NAMEWidgetView");
        renderer = new COMPONENT_NAMEWidget(this);
        renderer.start(renderableItem, (ViewGroup) findViewById(R.id.insert_point));

    }
}
