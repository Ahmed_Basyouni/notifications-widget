package com.backbase.android.PACKAGE_NAME;

import com.backbase.android.model.IconPack;
import com.backbase.android.model.ItemType;
import com.backbase.android.model.Renderable;
import com.backbase.android.model.inner.items.BBContent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Backbase R&D B.V on DATE_HOLDER.
 * Custom renderable to show the widget with the correct preferences
 */

public class CustomRenderable implements Renderable {
    private final Map<String, String> preferences = new HashMap<>();

    public CustomRenderable(String nativeView) {
        setPreference("native", "COMPONENT_NAMEWidget");
        setPreference("native.view", nativeView);
        setPreference("native.package", "com.backbase.android.widget.PACKAGE_NAME.views");
    }

    @Override
    public ItemType getType() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getPreference(String name) {
        return preferences.get(name);
    }

    @Override
    public Map<String, String> getPreferences() {
        return preferences;
    }

    @Override
    public void setPreference(String name, Object value) {
        preferences.put(name, (String) value);
    }

    @Override
    public List<Renderable> getChildren() {
        return null;
    }

    @Override
    public boolean hasPreload() {
        return false;
    }

    @Override
    public boolean hasRetain() {
        return false;
    }

    @Override
    public List<IconPack> getIcons() {
        return null;
    }

    @Override
    public IconPack getIconByName(String iconName) {
        return null;
    }

    @Override
    public IconPack getIconByIndex(int index) {
        return null;
    }

    @Override
    public Renderable getItemParent() {
        return null;
    }

    @Override
    public void setItemParent(Renderable parent) {
    }

    @Override
    public boolean isHtml() {
        return false;
    }

    @Override
    public BBContent getContent() {
        return null;
    }

    @Override
    public String getJsonObject() {
        return null;
    }
}
