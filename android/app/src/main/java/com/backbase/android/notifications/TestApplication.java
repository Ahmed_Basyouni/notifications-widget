package com.backbase.android.PACKAGE_NAME;

import android.app.Application;

import com.backbase.android.Backbase;
import com.backbase.android.core.utils.BBLogger;
import com.backbase.android.rendering.BBRenderer;
import com.backbase.android.widget.PACKAGE_NAME.core.COMPONENT_NAMEWidget;

/**
 * Created by Backbase R&D B.V on DATE_HOLDER.
 */
public class TestApplication extends Application {

    private static final String TAG = TestApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Backbase.setLogLevel(Backbase.LogLevel.DEBUG);
        BBRenderer.registerRenderer(COMPONENT_NAMEWidget.class);
        Backbase.initialize(this, "backbase/configuration.json", false);
        registerClients();
    }

    private void registerClients() {
        //TODO register Clients if needed
    }
}
