package com.backbase.android.widget.PACKAGE_NAME.core;

import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;

import com.backbase.android.model.Renderable;
import com.backbase.android.rendering.android.NativeRenderer;
import com.backbase.android.widget.PACKAGE_NAME.core.contract.COMPONENT_NAMEContract;
import com.backbase.android.widget.PACKAGE_NAME.core.contract.COMPONENT_NAMEView;

/**
 * Created by Backbase R&D B.V on DATE_HOLDER.
 */
public class COMPONENT_NAMEWidget extends NativeRenderer<COMPONENT_NAMEView> implements COMPONENT_NAMEContract {

    private final Context context;
    COMPONENT_NAMEView view;

    public COMPONENT_NAMEWidget(Context context) {
        this.context = context;
    }

    @Override
    public void start(Renderable renderable, ViewGroup viewGroup) {
        view = initializeView(context, renderable, viewGroup, this);
    }

    @Override
    public Renderable getRenderable() {
        return null;
    }

    @Override
    public Bundle saveInstanceState(Bundle bundle) {
        return null;
    }

    @Override
    public void restoreInstanceState(Bundle bundle) {
    }
}