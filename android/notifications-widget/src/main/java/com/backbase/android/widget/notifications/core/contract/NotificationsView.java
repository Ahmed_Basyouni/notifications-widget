package com.backbase.android.widget.PACKAGE_NAME.core.contract;

import com.backbase.android.rendering.android.NativeView;

/**
 * Created by Backbase R&D B.V on DATE_HOLDER.
 */
public interface COMPONENT_NAMEView extends NativeView<COMPONENT_NAMEContract> {

}