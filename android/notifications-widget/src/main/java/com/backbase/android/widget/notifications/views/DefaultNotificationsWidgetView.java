package com.backbase.android.widget.PACKAGE_NAME.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.backbase.android.widget.PACKAGE_NAME.R;
import com.backbase.android.widget.PACKAGE_NAME.core.contract.COMPONENT_NAMEContract;
import com.backbase.android.widget.PACKAGE_NAME.core.contract.COMPONENT_NAMEView;

/**
 * Created by Backbase R&D B.V on DATE_HOLDER.
 */
public class DefaultCOMPONENT_NAMEWidgetView extends View implements COMPONENT_NAMEView {

    public DefaultCOMPONENT_NAMEWidgetView(Context context) {
        super(context);
    }

    @Override
    public void init(COMPONENT_NAMEContract PACKAGE_NAMEContract, ViewGroup viewGroup) {
        View inflatedView = inflate(getContext(), R.layout.default_layout, viewGroup);

    }

    @Override
    public void onPause() {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onDestroy() {
    }
}